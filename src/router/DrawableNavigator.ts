// eslint-disable-next-line no-unused-vars
import { RouterTypes } from './routerTypes';
import { GenerateUID } from '../utils/random';

// import components here
import {
    About,
} from '../ui/pages';

import TabComponent from '../ui/components/molecules/navigation/Tab';

const DrawerRoute:RouterTypes = [
    {
        id: GenerateUID(),
        name: 'About',
        component: About,
        options: {
            headerShown: false,
        },
    },
    {
        id: GenerateUID(),
        name: 'Tab',
        component: TabComponent,
        options: {
            headerShown: false,
        },
    },
];

export default DrawerRoute;
