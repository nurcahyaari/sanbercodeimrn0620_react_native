// eslint-disable-next-line no-unused-vars
import { RouterTypes } from './routerTypes';
import { GenerateUID } from '../utils/random';

// import components here
import {
    SkillScreen,
    AddScreen,
    ProyekScreen,
} from '../ui/pages';

const TabRoute:RouterTypes = [
    {
        id: GenerateUID(),
        name: 'SkillScreen',
        component: SkillScreen,
        options: {
            headerShown: false,
        },
    },
    {
        id: GenerateUID(),
        name: 'ProyekScreen',
        component: ProyekScreen,
        options: {
            headerShown: false,
        },
    },
    {
        id: GenerateUID(),
        name: 'AddScreen',
        component: AddScreen,
        options: {
            headerShown: false,
        },
    },
];

export default TabRoute;
