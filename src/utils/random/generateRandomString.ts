/* eslint-disable import/prefer-default-export */
export function GenerateUID():string {
    return (new Date().getTime() + Math.floor((Math.random() * 10000) + 1)).toString();
}
