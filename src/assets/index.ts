const YoutubeLogo = require('./images/logo.png');
const DataYoutube = require('./data.json');

const LoginIlustration = require('./images/login.png');
const FacebookLogo = require('./images/facebooklogo.png');
const LinkedInLogo = require('./images/linkedin.png');
const GithubLogo = require('./images/githulogo.png');
const ProfileImage = require('./images/profile.jpg');

const GolangLogo = require('./images/golang.png');
const JSLogo = require('./images/js.png');
const TSLogo = require('./images/ts.png');
const CPPLogo = require('./images/c++.png');
const PHPLogo = require('./images/php.png');
const PythonLogo = require('./images/python.png');
const FastifyLogo = require('./images/fastify.png');
const NestLogo = require('./images/nest.png');
const FiberGoLogo = require('./images/fiber.png');
const CILogo = require('./images/ci.png');
const GitLabLogo = require('./images/gitlab.png');
const ExpressLogo = require('./images/express.png');
const DockerLogo = require('./images/docker.png');

export { YoutubeLogo };
export { DataYoutube };

export { LoginIlustration };
export { FacebookLogo };
export { GithubLogo };
export { LinkedInLogo };
export { ProfileImage };
export {
    GolangLogo,
    JSLogo,
    ExpressLogo,
    TSLogo,
    CPPLogo,
    CILogo,
    PHPLogo,
    PythonLogo,
    FastifyLogo,
    NestLogo,
    FiberGoLogo,
    GitLabLogo,
    DockerLogo,
};
