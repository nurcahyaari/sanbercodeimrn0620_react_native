import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerRoute from '../../../../router/DrawableNavigator';

const Drawer = createDrawerNavigator();

const DrawerComponent = () => (
  <Drawer.Navigator initialRouteName="About">
    {
      DrawerRoute
      .map((data) => (
        <Drawer.Screen
          key={data.id}
          name={data.name}
          component={data.component}
        />
      ))
    }
  </Drawer.Navigator>
);

export default DrawerComponent;
