import React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabRoute from '../../../../router/TabNavigator';

const Tab = createBottomTabNavigator();


const TabComponent = () => (
  <Tab.Navigator>
    {
        TabRoute
        .map((data) => <Tab.Screen key={data.id} name={data.name} component={data.component} />)
      }
  </Tab.Navigator>
);

export default TabComponent;
