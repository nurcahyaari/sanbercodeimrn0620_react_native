import DrawerComponent from './Drawable';
import TabComponent from './Tab';

export {
    DrawerComponent,
    TabComponent,
};
