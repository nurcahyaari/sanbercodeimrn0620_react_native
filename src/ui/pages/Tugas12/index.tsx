/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
 View, StyleSheet, Image, TouchableOpacity, Text, FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

// import costum components
import VideoItem from './components/VideoItems';
// import assets
import { YoutubeLogo, DataYoutube } from '../../../assets';

const Youtube = () => (
  <View style={styles.container}>
    <View style={styles.navBar}>
      <Image style={styles.youtubeLogo} source={YoutubeLogo} />
      <View style={styles.rightNav}>
        <TouchableOpacity>
          <Icon style={styles.navItem} name="search" size={25} />
        </TouchableOpacity>
        <TouchableOpacity>
          <Icon style={styles.navItem} name="account-circle" size={25} />
        </TouchableOpacity>
      </View>
    </View>
    <View style={styles.body}>
      <FlatList
        data={DataYoutube.items}
        renderItem={(video) => <VideoItem video={video.item} />}
        keyExtractor={(item) => item.id}
        ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
      />
    </View>
    <View style={styles.tabBar}>
      <TouchableOpacity style={styles.tabItem}>
        <Icon name="home" size={25} />
        <Text style={styles.tabTitle}>Home</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem}>
        <Icon name="whatshot" size={25} />
        <Text style={styles.tabTitle}>Trending</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem}>
        <Icon name="subscriptions" size={25} />
        <Text style={styles.tabTitle}>Subscriptions</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.tabItem}>
        <Icon name="folder" size={25} />
        <Text style={styles.tabTitle}>Library</Text>
      </TouchableOpacity>
    </View>
  </View>
);

const styles = StyleSheet.create({
    body: {
        flex: 1,
    },
    container: {
        flex: 1,
    },
    navBar: {
        alignItems: 'center',
        backgroundColor: '#ffffff',
        elevation: 3,
        flexDirection: 'row',
        height: 55,
        justifyContent: 'space-between',
        paddingHorizontal: 15,
    },
    navItem: {
        marginLeft: 25,
    },
    rightNav: {
        flexDirection: 'row',
    },
    tabBar: {
        backgroundColor: 'white',
        borderColor: '#E5E5E5',
        borderTopWidth: 0.5,
        flexDirection: 'row',
        height: 60,
        justifyContent: 'space-around',
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    tabTitle: {
        color: '#3c3c3c',
        fontSize: 11,
        paddingTop: 4,
    },
    youtubeLogo: {
        height: 22,
        width: 98,
    },
});

export default Youtube;
