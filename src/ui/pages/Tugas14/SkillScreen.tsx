import React, { useState } from 'react';
import {
    Text,
    View,
    Image,
    FlatList,
} from 'react-native';

import {
    ProfileImage,
    GolangLogo,
    JSLogo,
    TSLogo,
    CPPLogo,
    PHPLogo,
    PythonLogo,
    FastifyLogo,
    NestLogo,
    ExpressLogo,
    FiberGoLogo,
    CILogo,
    GitLabLogo,
    GithubLogo,
    DockerLogo,
} from '../../../assets';

import { List } from './components';

// eslint-disable-next-line no-unused-vars
import { ISkill } from './dto/SkillListDTO';

const SkillSreen = () => {
    // eslint-disable-next-line no-unused-vars
    const [skill, setSkill] = useState<ISkill[] | undefined>([
        {
            id: 1,
            name: 'Basic Golang',
            percentage: 25,
            image: GolangLogo,
            type: 'Programming Language',
        },
        {
            id: 2,
            name: 'Advandce Javascript',
            percentage: 90,
            image: JSLogo,
            type: 'Programming Language',
        },
        {
            id: 3,
            name: 'Advandce Typescript',
            percentage: 85,
            image: TSLogo,
            type: 'Programming Language',
        },
        {
            id: 4,
            name: 'Basic C++',
            percentage: 25,
            image: CPPLogo,
            type: 'Programming Language',
        },
        {
            id: 5,
            name: 'Advandce PHP',
            percentage: 80,
            image: PHPLogo,
            type: 'Programming Language',
        },
        {
            id: 6,
            name: 'Basic Python',
            percentage: 15,
            image: PythonLogo,
            type: 'Programming Language',
        },
        {
            id: 7,
            name: 'Intermediate Fastify',
            percentage: 70,
            image: FastifyLogo,
            type: 'Frameworks',
        },
        {
            id: 8,
            name: 'Intermediate NestJS',
            percentage: 75,
            image: NestLogo,
            type: 'Frameworks',
        },
        {
            id: 9,
            name: 'Advandce ExpressJS',
            percentage: 80,
            image: ExpressLogo,
            type: 'Frameworks',
        },
        {
            id: 10,
            name: 'Basic Golang',
            percentage: 25,
            image: GolangLogo,
            type: 'Frameworks',
        },
        {
            id: 11,
            name: 'Basic fibergo',
            percentage: 25,
            image: FiberGoLogo,
            type: 'Frameworks',
        },
        {
            id: 12,
            name: 'Advance CI3',
            percentage: 80,
            image: CILogo,
            type: 'Frameworks',
        },
        {
            id: 13,
            name: 'Intermediate Gitlab',
            percentage: 65,
            image: GitLabLogo,
            type: 'Tools',
        },
        {
            id: 14,
            name: 'Intermediate Github',
            percentage: 25,
            image: GithubLogo,
            type: 'Tools',
        },
        {
            id: 15,
            name: 'Basic Docker',
            percentage: 10,
            image: DockerLogo,
            type: 'Tools',
        },
    ]);

    return (
      <View>
        <View>
          <FlatList
            data={skill}
            renderItem={({ item }) => {
                if ([1, 7, 13].includes(item.id)) {
                    return (
                      <View>
                        <Text style={{ marginVertical: 25, fontSize: 16, fontWeight: 'bold' }}>{item.type}</Text>
                        <List {...item} />
                      </View>
                    );
                }
                return (
                  <List {...item} />
                );
            }}
            keyExtractor={(item) => item.id.toString()}
            contentContainerStyle={{
                padding: 21,
            }}
            ListHeaderComponent={() => (
              <View>
                <View style={{ flexDirection: 'row', marginTop: 50, alignItems: 'center' }}>
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                      source={ProfileImage}
                      style={{
                        width: 128,
                        height: 128,
                        borderRadius: 64,
                      }}
                    />
                  </View>
                  <View style={{ justifyContent: 'space-between' }}>
                    <Text style={{ padding: 5, fontSize: 16, fontWeight: 'bold' }}>Ari Nurcahya</Text>
                    <Text style={{ padding: 5 }}>Software Engineer</Text>
                  </View>
                </View>
              </View>
              )}
          />
        </View>
      </View>
      );
};

export default SkillSreen;
