import React, { useState } from 'react';
import {
 View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet,
} from 'react-native';

import { Note } from './components';


const MainNote = () => {
    const [notes, setNotes] = useState([
        {
          id: 1,
          skillName: 'React Native',
          category: 'Library',
          categoryName: 'Framework / Library',
          logoUrl: '',
          iconType: 'MaterialCommunityIcons',
          iconName: 'react',
          percentageProgress: '50%',
        },
        {
          id: 2,
          skillName: 'Laravel',
          category: 'Library',
          categoryName: 'Framework / Library',
          logoUrl: '',
          iconType: 'MaterialCommunityIcons',
          iconName: 'laravel',
          percentageProgress: '100%',
        },
        {
          id: 3,
          skillName: 'JavaScript',
          category: 'Language',
          categoryName: 'Bahasa Pemrograman',
          logoUrl: '',
          iconType: 'MaterialCommunityIcons',
          iconName: 'language-javascript',
          percentageProgress: '30%',
        },
        {
          id: 4,
          skillName: 'Python',
          category: 'Language',
          categoryName: 'Bahasa Pemrograman',
          logoUrl: '',
          iconType: 'MaterialCommunityIcons',
          iconName: 'language-python',
          percentageProgress: '70%',
        },
        {
          id: 5,
          skillName: 'Git',
          category: 'Technology',
          categoryName: 'Teknologi',
          logoUrl: '',
          iconType: 'MaterialCommunityIcons',
          iconName: 'git',
          percentageProgress: '75%',
        },
        {
          id: 6,
          skillName: 'Gitlab',
          category: 'Technology',
          categoryName: 'Teknologi',
          logoUrl: '',
          iconType: 'MaterialCommunityIcons',
          iconName: 'gitlab',
          percentageProgress: '60%',
        },
    ]);
    const [noteText, setNoteText] = useState('');

    const DeleteNote = (id: number) => {
        notes.splice(id, 1);
        setNotes([...notes]);
    };

    const AddNote = () => {
        if (noteText) {
            notes.push({
                id: notes.length + 1,
                skillName: noteText,
                category: 'Technology',
                categoryName: 'Teknologi',
                logoUrl: '',
                iconType: 'MaterialCommunityIcons',
                iconName: 'gitlab',
                percentageProgress: '60%',
            });
            setNotes([...notes]);
            setNoteText('');
        }
    };


    const RenderNotes = () => notes.map((item, key) => (
      <Note
        // eslint-disable-next-line react/no-array-index-key
        key={item.id}
        value={item}
        onDelete={() => DeleteNote(key)}
      />
    ));
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>- Note -</Text>
        </View>

        <ScrollView style={styles.scrollContainer}>
          {RenderNotes()}
        </ScrollView>

        <View style={styles.footer}>
          <TextInput
            style={styles.textInput}
            placeholder=">Note"
            placeholderTextColor="#fff"
            underlineColorAndroid="transparent"
            value={noteText}
            onChangeText={(value) => setNoteText(value)}
          />
        </View>

        <TouchableOpacity
          style={styles.addButton}
          onPress={AddNote}
        >
          <Text style={styles.addButtonText}>+</Text>
        </TouchableOpacity>
      </View>
    );
};

export default MainNote;

const styles = StyleSheet.create({
  addButton: {
    alignItems: 'center',
    backgroundColor: '#E91E63',
    borderRadius: 50,
    bottom: 90,
    elevation: 8,
    height: 90,
    justifyContent: 'center',
    position: 'absolute',
    right: 20,
    width: 90,
    zIndex: 11,
  },
  addButtonText: {
    color: '#fff',
    fontSize: 24,
  },
  container: {
    flex: 1,
  },
  footer: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    right: 0,
    zIndex: 10,
  },
  header: {
    alignItems: 'center',
    backgroundColor: '#E91E63',
    borderBottomColor: '#ddd',
    borderBottomWidth: 10,
    justifyContent: 'center',
  },
  headerText: {
    color: 'white',
    fontSize: 18,
    padding: 26,
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  },
  textInput: {
    alignSelf: 'stretch',
    backgroundColor: '#252525',
    borderTopColor: '#ededed',
    borderTopWidth: 2,
    color: '#fff',
    padding: 20,
  },
});
