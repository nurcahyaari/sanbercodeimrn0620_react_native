import React from 'react';
import {
 View, Text, TouchableOpacity, StyleSheet,
} from 'react-native';

interface PropsType {
    key: number;
    value: any,
    onDelete: () => void,
}

const Note = (props:PropsType) => {
    const { key, value, onDelete } = props;
    return (
      <View style={styles.note} key={key}>
        <Text style={styles.noteText}>
          {value.skillName}
        </Text>
        <Text style={styles.noteText}>
          {value.category}
        </Text>
        <TouchableOpacity style={styles.noteDelete} onPress={onDelete}>
          <Text style={styles.noteDeleteText}>D</Text>
        </TouchableOpacity>
      </View>
    );
};

export default Note;


const styles = StyleSheet.create({
  note: {
    borderBottomColor: '#ededed',
    borderBottomWidth: 2,
    padding: 20,
    paddingRight: 100,
    position: 'relative',
  },
  noteDelete: {
    alignItems: 'center',
    backgroundColor: '#2980b9',
    bottom: 10,
    justifyContent: 'center',
    padding: 10,
    position: 'absolute',
    right: 10,
    top: 10,
  },
  noteDeleteText: {
    color: 'white',
  },
  noteText: {
    borderLeftColor: '#e91e63',
    borderLeftWidth: 10,
    paddingLeft: 20,
  },
});
