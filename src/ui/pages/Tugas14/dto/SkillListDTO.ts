export interface ISkill {
    id: number;
    name: string;
    percentage: number;
    image: any;
    type: string;
}
