import React, { useState } from 'react';
import {
 View, Text, Image, Linking, FlatList, TouchableOpacity,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { GenerateUID } from '../../../utils/random';
import {
 ProfileImage, FacebookLogo, GithubLogo, LinkedInLogo,
} from '../../../assets';
// custom component
import {
    Button,
} from '../../components/atoms';
import { ListPortofolio } from './components';

// dto
// eslint-disable-next-line no-unused-vars
import { PortofolioDTO } from './dto/PortofolioDTO';

const About = () => {
    const navigator = useNavigation();
    // eslint-disable-next-line no-unused-vars
    const [portofolio, setPortofolio] = useState<PortofolioDTO[] | undefined>([
        {
            id: GenerateUID(),
            title: 'Laptipikor.com',
            description: 'is a web based for complaining to  local goverment',
            url: 'http://lapdutipikor.com/',
        },
        {
            id: GenerateUID(),
            title: 'Jwt-token-manager',
            description: 'Simple library for saving jwt refresh-token to local files (.txt file)',
            url: 'https://github.com/NurcahyaAri/jwt-token-manager',
        },
        {
            id: GenerateUID(),
            title: 'sd muhammadiyah trisigan web profile',
            description: 'Web profile for sd muhammadiyah trisigan, located at Bantul, Yogyakarta',
            url: 'https://sdmuhtrisigan.sch.id/',
        },
        {
            id: GenerateUID(),
            title: 'Vector-Space-Model',
            description: 'calculate similarity document using vector-space-model. but it’s not production ready',
            url: 'https://github.com/NurcahyaAri/Vector-Space-Model-using-Cosine-Similarity',
        },
        {
            id: GenerateUID(),
            title: 'Golang Starter',
            description: 'Golang boilerplate, base on fibergo framework. this boilerplate inspired by clean code architecture',
            url: 'https://github.com/NurcahyaAri/golang-starter',
        },
        {
            id: GenerateUID(),
            title: 'Gunungapipurba competition',
            description: 'My first project with my lecturer. this is using instagram API. then collect data with hastag #gunungapipurba and save to localdb (.json file). I’ve done this project in 2017',
            url: '',
        },
        {
            id: GenerateUID(),
            title: 'Worked at UNIQ',
            description: 'currently i’m working here. I was creating CRM App, Service for user billing. Agent app Chat support, HRM App, Finance App. \n now I want to migrate my Agent App from sailsjs to golang (API) and React/ React Native for FrontEnd.. ',
            url: 'https://www.uniq.id/',
        },
    ]);
    return (
      <View>
        <View>
          <FlatList
            data={portofolio}
            renderItem={({ item }) => <ListPortofolio {...item} />}
            keyExtractor={(item) => item.id}
            contentContainerStyle={{
              padding: 21,
            }}
            ListHeaderComponent={() => (
              <View>
                <View style={{ flexDirection: 'row', marginTop: 50, alignItems: 'center' }}>
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                      source={ProfileImage}
                      style={{
                            width: 128,
                            height: 128,
                            borderRadius: 64,
                        }}
                    />
                  </View>
                  <View style={{ justifyContent: 'space-between' }}>
                    <Text style={{ padding: 5, fontSize: 16, fontWeight: 'bold' }}>Ari Nurcahya</Text>
                    <Text style={{ padding: 5 }}>Software Engineer</Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row', flex: 1, justifyContent: 'space-between', marginTop: 30,
                  }}
                >
                  <View>
                    <View>
                      <Image source={FacebookLogo} style={{ width: 64, height: 64 }} />
                    </View>
                    <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/nurcahyaari/')}>
                      <Text>Ari Nurcahya</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <View>
                      <Image source={GithubLogo} style={{ width: 64, height: 64 }} />
                    </View>
                    <TouchableOpacity onPress={() => Linking.openURL('https://github.com/NurcahyaAri')}>
                      <Text>nurcahyaari</Text>
                    </TouchableOpacity>
                  </View>
                  <View>
                    <View>
                      <Image source={LinkedInLogo} style={{ width: 64, height: 64 }} />
                    </View>
                    <TouchableOpacity onPress={() => Linking.openURL('https://www.linkedin.com/in/nurcahyaari/')}>
                      <Text>Ari Nurcahya</Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{ marginVertical: 30 }}>
                  <Button
                    style={{ backgroundColor: '#F1C961' }}
                    title="Here is my skill list"
                    onPress={() => navigator.navigate('Tab')}
                  />
                </View>
              </View>
            )}
          />
        </View>
      </View>
      );
};

export default About;
