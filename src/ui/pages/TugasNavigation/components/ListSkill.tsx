import React from 'react';
import {
 View, Text, Image, Dimensions,
} from 'react-native';
// eslint-disable-next-line no-unused-vars
import { ISkill } from '../dto/SkillListDTO';

// eslint-disable-next-line no-unused-vars
// import { PortofolioDTO } from '../dto/PortofolioDTO';

const List = (props: ISkill) => {
    const {
        id,
        name,
        percentage,
        image,
    } = props;
    return (
      <View
        key={id}
        style={{
              marginVertical: 1,
              padding: 5,
              flexDirection: 'column',
              flex: 1,
        }}
      >
        <View style={{ flexDirection: 'row', flex: 1 }}>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Image source={image} style={{ width: 54, height: 54, resizeMode: 'contain' }} />
          </View>
          <View style={{ justifyContent: 'space-evenly', flex: 1, padding: 10 }}>
            <Text style={{ flex: 1, flexWrap: 'wrap' }}>{name}</Text>
          </View>
        </View>
        <View
          style={{
                flex: 1,
                backgroundColor: '#C4C4C4',
                height: 13,
                marginVertical: 5,
          }}
        >
          <View
            style={{
                flex: 1,
                width: `${percentage}%`,
                backgroundColor: '#61EC4B',
                height: 13,
            }}
          />
          <Text
            style={{
                position: 'absolute',
                fontSize: 11,
                left: Dimensions.get('window').width / 2.5,
                justifyContent: 'center',
                alignItems: 'center',
            }}
          >
            {percentage}
            %
          </Text>
        </View>
      </View>
      );
};

export default List;
