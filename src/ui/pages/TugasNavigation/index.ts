import Login from './login';
import About from './about';
import AddScreen from './AddScreen';
import ProyekScreen from './ProyekScreen';
import SkillScreen from './SkillScreen';

export {
    Login,
    About,
    AddScreen,
    ProyekScreen,
    SkillScreen,
};
