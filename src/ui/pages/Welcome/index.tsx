/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
 Text,
 View,
} from 'react-native';

const Welcome = () => (
  <View
    style={{
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    }}
  >
    <Text
      style={{
        fontWeight: 'bold',
        fontSize: 28,
      }}
    >
      Hello World
    </Text>
  </View>
);

export default Welcome;
