import React from 'react';
import {
 View, Text, TouchableOpacity, Linking,
} from 'react-native';

// eslint-disable-next-line no-unused-vars
import { PortofolioDTO } from '../dto/PortofolioDTO';

const List = (props: PortofolioDTO) => {
    const { url, description, title } = props;
    return (
      <View
        style={{
              backgroundColor: '#F6F6F6',
              marginVertical: 6,
              padding: 15,
              flexDirection: 'row',
              flex: 1,
        }}
      >
        <View style={{ width: 70, height: 10 }} />
        <View style={{ justifyContent: 'space-evenly', flex: 1, padding: 10 }}>
          <TouchableOpacity onPress={() => Linking.openURL(url)}>
            <Text style={{ marginBottom: 10, borderBottomColor: '#000', borderBottomWidth: 1 }}>{title}</Text>
          </TouchableOpacity>
          <Text style={{ flex: 1, flexWrap: 'wrap' }}>{description}</Text>
        </View>
      </View>
      );
};

export default List;
