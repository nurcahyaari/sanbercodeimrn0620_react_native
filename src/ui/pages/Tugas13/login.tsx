import React from 'react';
import { View, Image } from 'react-native';

// import custom components
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
// import colors from '../../components/colors';
import {
  TextInput,
  Button,
  SizedBox,
} from '../../components/atoms';

// import image
import { LoginIlustration } from '../../../assets';

const Login = () => {
    const navigator = useNavigation();
    return (
      <ScrollView contentContainerStyle={{ padding: 25 }}>
        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
          <Image source={LoginIlustration} style={{ width: 250, height: 228 }} />
        </View>
        <SizedBox margin={50} />
        <TextInput placeholder="Username" />
        <SizedBox margin={20} />
        <TextInput placeholder="Password" secureTextEntry />
        <SizedBox margin={20} />
        <Button
          style={{ backgroundColor: '#F1C961' }}
          onPress={() => navigator.navigate('About')}
          title="Login"
        />
      </ScrollView>
      );
};

export default Login;
