export interface PortofolioDTO {
    id: string;
    title: string;
    description: string;
    url: string;
}
